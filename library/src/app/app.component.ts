import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <div style="text-align:center">
  <h1>
    {{title}} LIBRARY
  </h1>
  </div>
  <app-library></app-library>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'JANETO';
}

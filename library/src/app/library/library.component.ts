import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-library',
  templateUrl: 'library.component.html',
  styleUrls: ['library.component.css']
})
export class LibraryComponent implements OnInit {

  books: Book[];
  textSearch: String;
  selected: Number = -1;
  constructor() { }

  ngOnInit() {
    this.books = [{ author: 'laptrinhvien', id: 1, image: '/assets/angular-cover.jpg', title: 'Angular Cơ Bản' },
    { author: 'Janeto', id: 2, image: '/assets/front-end-cover.jpeg', title: 'Front-End Từ A - Z' },
    { author: 'laptrinhvien', id: 3, image: '/assets/node-cover.jpeg', title: 'Nodejs Toàn Tập' },
    { author: 'quynhlx', id: 4, image: '/assets/cpp-cover.jpg', title: 'Lập Trình C++' }];
  }

  clearBook() {
    this.books = undefined;
  }

  selectBook(book: Book) {
    this.selected = book.id;
  }

  search() {
    console.log(this.textSearch);
  }
}

interface Book {
  title: String;
  author: String;
  image: String;
  id: Number;
}
